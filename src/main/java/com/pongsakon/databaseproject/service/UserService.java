/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pongsakon.databaseproject.service;

import com.pongsakon.databaseproject.dao.UserDao;
import com.pongsakon.databaseproject.model.User;

/**
 *
 * @author 66955
 */
public class UserService {
    public User login(String name,String password){
        UserDao userDao = new UserDao();
        User user = userDao.getName(name);
        if(user != null && user.getPassword().equals(password)){
            return user;
        }
        return null;
    }

}
